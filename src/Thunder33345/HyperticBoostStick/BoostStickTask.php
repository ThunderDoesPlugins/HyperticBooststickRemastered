<?php
declare(strict_types=1);
/* Made By Thunder33345 */
namespace Thunder33345\HyperticBoostStick;

use pocketmine\item\Item;
use pocketmine\scheduler\Task;
use pocketmine\utils\TextFormat as Text;

class BoostStickTask extends Task
{
  private $boostStick, $server;
  public $barLength = 35;
  public $barCode = '-';

  public function __construct(BoostStick $boostStick)
  {
    $this->boostStick = $boostStick;
    $this->server = $boostStick->getServer();
  }

  public function onRun($currentTick)
  {
    $server = $this->server;
    $boostStick = $this->boostStick;
    foreach($server->getOnlinePlayers() as $player){
      if($player->getInventory()->getItemInHand()->getId() == Item::BLAZE_ROD){
        $player->sendTip("Yaw: ".round($player->getYaw(), 5)."\nPitch: ".round($player->getPitch(), 5));
        return;
      }
      if(!$boostStick->isBoostStick($player)) return;
      $name = $player->getLowerCaseName();

      $mode = $boostStick->getModeByPitch($player->getPitch());

      $charge = $boostStick->getRemainingCharge($name);
      $totalCharge = $boostStick->defaultCharge;

      $coolDown = round($boostStick->getFirstWait($name), 3);;
      $totalWait = round($boostStick->getFirstWait($name, true), 3);

      $info = "Mode: ".ucfirst($mode);
      if($totalWait > 1){
        $info .= ' Total Wait: '.$totalWait;
      }
      $info .= "\n";

      $totalCoolDown = $boostStick->defaultCoolDown;

      $coolDownBar = $this->calculateBars($this->barLength, $totalCoolDown, $coolDown, true);
      $chargeBar = $this->calculateBars($this->barLength, $totalCharge, $charge);

      //@formatter:off
      $bar = $this->renderBars(
       $this->barLength, $coolDownBar, $chargeBar,
       Text::GREEN, Text::YELLOW, Text::GOLD, Text::GRAY
      );
      //@formatter:on
      $info .= '|'.$bar.Text::WHITE.'|';

      $info .= "\nCool down bar: $coolDownBar - Charge bar: $chargeBar";

      $player->sendPopup(Text::RESET.StringUtils::center($info));
      /*
      $coolDown = round($boostStick->getFirstWait($name), 4);
      if($coolDown <= 0) $coolDown = "Ready";

      $charge = $boostStick->getRemainingCharge($name);

      $total = round($boostStick->getFirstWait($name, true), 4);
      if($total <= 0) $total = 'All Ready';

      if($boostStick->isClickLimited($name)) $protect = 'Protected!'; else $protect ='Not Protected';

      $player->sendPopup(
       " \n".
       'Mode: '.ucfirst($mode)."\n".'Cool down: '.$coolDown.' Charge: '.$charge.
       "\nTotal Wait: ".$total.' Protected: '.$protect);

      */
    }

  }

  public function renderBars(int $length, int $bar1Count, int $bar2Count, string $bar1Color, string $bar2Color, string $mergedColor, string $emptyColor)
  {
    $bar = '';
    $last = '';
    for($i = 1; $i <= $length; $i++){
      if($bar1Count >= $i and $bar2Count >= $i){
        if($last != $mergedColor){
          $bar .= $mergedColor;
          $last = $mergedColor;
        }
      }elseif($bar1Count >= $i){
        if($last != $bar1Color){
          $bar .= $bar1Color;
          $last = $bar1Color;
        }
      }elseif($bar2Count >= $i){
        if($last != $bar2Color){
          $bar .= $bar2Color;
          $last = $bar2Color;
        }
      }else{
        if($last != $emptyColor){
          $bar .= $emptyColor;
          $last = $emptyColor;
        }
      }
      $bar .= $this->barCode;
    }
    return $bar;
  }

  public function calculateBars(int $length, float $total, float $subtotal, $invert = false)
  {
    $percentage = $this->getPercentage($total, $subtotal);
    $filled = intval(round($this->getPercentByNumber($length, $percentage), 0), PHP_ROUND_HALF_UP);
    if($invert) return $length - $filled;
    return $filled;
  }

  public function getPercentage(float $total, float $subtotal):float
  {
    return $subtotal / $total * 100;
  }

  public function getPercentByNumber($number, $percent)
  {
    return ($percent / 100) * $number;
  }
}