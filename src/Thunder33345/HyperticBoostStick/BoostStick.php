<?php
declare(strict_types=1);
/** Created By Thunder33345 **/
namespace Thunder33345\HyperticBoostStick;

use pocketmine\command\Command;
use pocketmine\command\CommandSender;
use pocketmine\event\Listener;
use pocketmine\event\player\PlayerInteractEvent;
use pocketmine\inventory\PlayerInventory;
use pocketmine\item\Item;
use pocketmine\math\Vector3;
use pocketmine\Player;
use pocketmine\plugin\PluginBase;
use pocketmine\utils\TextFormat;

class BoostStick extends PluginBase implements Listener
{
  const MODE_DASH = 'dash';
  const MODE_CHARGE = 'charge';
  const MODE_LEAP = 'leap';

  public $coolDowns = [];
  private $charge = [];

  public $defaultCharge = 15;
  public $defaultCoolDown = 2;
  public $defaultClickProtect = 0.3;

  public function onLoad()
  {

  }

  public function onEnable()
  {
    $this->getServer()->getPluginManager()->registerEvents($this, $this);
    $this->getScheduler()->scheduleRepeatingTask(new BoostStickTask($this), 2);
  }

  public function onDisable()
  {

  }

  public function onCommand(CommandSender $sender, Command $command, string $label, array $args):bool
  {
    return true;
  }

  public function onBoost(PlayerInteractEvent $interactEvent)
  {
    $player = $interactEvent->getPlayer();
    if($interactEvent->getAction() !== PlayerInteractEvent::RIGHT_CLICK_BLOCK AND $interactEvent->getAction() !== PlayerInteractEvent::RIGHT_CLICK_AIR){
      return;
    }
    if(!$this->isBoostStick($player)) return;
    $name = $player->getLowerCaseName();
    if($this->isClickLimited($name)) return;
    if(!$this->getLazyCanUseCharge($name)) return;
    /*
    low - dash
    mid - charge
    high - leap
    hop, rush
    */

    $planeVector = $player->getDirectionPlane();
    $knockVector = new Vector3();

    $mode = $this->getModeByPitch($player->getPitch());
    switch($mode){
      case self::MODE_DASH://low
        $planeVector = $planeVector->multiply(1.85);
        $knockVector->y = 0.30;
        $knockVector->x = $planeVector->x;
        $knockVector->z = $planeVector->y;
        break;
      case self::MODE_CHARGE://mid
        $planeVector = $planeVector->multiply(1.15);
        $knockVector->y = 0.60;
        $knockVector->x = $planeVector->x;
        $knockVector->z = $planeVector->y;
        break;
      case self::MODE_LEAP://high
        $planeVector = $planeVector->divide(1.40);
        $knockVector->y = 1.10;
        $knockVector->x = $planeVector->x;
        $knockVector->z = $planeVector->y;
        break;
    }
    $player->setMotion($knockVector);

    $this->recordChargeUsed($name);
    $this->registerClick($name);
  }

  public function isClickLimited($name):bool
  {
    $this->resolveName($name);
    if(!isset($this->charge[$name]['click'])) return false;

    $endTime = $this->charge[$name]['click'];
    $now = microtime(true);

    if($endTime > $now) return true;else return false;
  }

  public function registerClick($name):void
  {
    $this->resolveName($name);
    $now = microtime(true);
    $this->charge[$name]['click'] = ($now + $this->defaultClickProtect);
  }

  /*
  public function setCoolDown($name, float $time)
  {
    if($name instanceof Player) $name = $name->getLowerCaseName();
    if(!is_string($name)){
      throw new \UnexpectedValueException("\$name expected string got $name");
    }
    $this->coolDowns[$name] = (float)microtime(true) + $time;
  }

  public function isOnCoolDown($name)
  {
    if($name instanceof Player) $name = $name->getLowerCaseName();
    if(!is_string($name)){
      throw new \UnexpectedValueException("\$name expected string got $name");
    }
    if(isset($this->coolDowns[$name])){
      $finish = $this->coolDowns[$name];
      if($finish > microtime(true)){
        return true;
      }else return false;
    }else return false;
  }

  public function getCoolDown($name)
  {
    if($name instanceof Player) $name = $name->getLowerCaseName();
    if(!is_string($name)){
      throw new \UnexpectedValueException("\$name expected string got $name");
    }
    if(!$this->isOnCoolDown($name)) return 0;
    $finish = $this->coolDowns[$name];
    return (float)$finish - microtime(true);
  }
  */
  //todo charges
  //charge is in an array ["name"=>[time()'s]]
  //each time is when an charge is taken away
  //old charges get cleared when caculating for it

  public function cleanCharge($name):void
  {
    $this->resolveName($name);
    if(isset($this->charge[$name]['table'])) foreach($this->charge[$name]['table'] as $key => $finishTime){
      if(microtime(true) > $finishTime) unset($this->charge[$name]['table'][$key]);
    }
  }

  public function getRemainingCharge($name, $used = false):int
  {
    $this->resolveName($name);

    if(!isset($this->charge[$name]['table'])) return $this->defaultCharge;

    $this->cleanCharge($name);
    if($used === false){
      return $this->defaultCharge - count($this->charge[$name]['table']);
    }else return count($this->charge[$name]['table']);
  }

  public function getLazyCanUseCharge($name):bool
  {
    $this->resolveName($name);

    if(!isset($this->charge[$name]['table'])) return true;

    $table = $this->charge[$name]['table'];
    if($this->defaultCharge > count($table)) return true;else{
      $remaining = $this->getRemainingCharge($name);
      if($remaining >= 1) return true;
      return false;
    }
  }

  public function recordChargeUsed($name):bool
  {
    $this->resolveName($name);

    $table = &$this->charge[$name]['table'];
    if(!$this->getLazyCanUseCharge($name)) return false;
    $time = microtime(true);

    if(!isset($this->charge[$name]['table']) OR count($this->charge[$name]['table']) === 0) $last = 0;else{
      $last = array_values(array_slice($table, -1))[0];
    }

    if($last > $time) $time = $last;

    $table[] = $time + $this->defaultCoolDown;
    return true;
  }

  public function resolveName(&$name):string
  {
    if(is_string($name)) $name = strtolower($name);elseif($name instanceof Player) $name = $name->getLowerCaseName();
    else throw new \UnexpectedValueException("\$name expected string got $name");
    return $name;
  }

  public function getFirstWait($name, $total = false):float
  {
    $this->resolveName($name);
    $this->cleanCharge($name);
    if(!isset($this->charge[$name]['table']) or count($this->charge[$name]['table']) <= 0) return 0;
    $table = $this->charge[$name]['table'];
    if($total === false){
      $first = reset($table);
      if(microtime(true) > $first){
        return 0;
      }else return $first - microtime(true);
    }else{
      $last = array_values(array_slice($table, -1))[0];
      if($last > microtime(true)) return $last - microtime(true);
      return 0;
    }
  }

  public function isBoostStick($thing)
  {
    if($thing instanceof Player){
      $thing = $thing->getInventory()->getItemInHand();
    }elseif($thing instanceof PlayerInventory){
      $thing = $thing->getItemInHand();
    }
    if(!$thing instanceof Item){
      throw new \UnexpectedValueException("Unknown thing, thing must be Item or resolvable into item");
    }

    $item = $thing;
    if($item->getId() == Item::STICK) return true;
    return false;
  }

  public function getModeByPitch(float $pitch)
  {
    if($pitch < -90){
      $pitch = -90;
      $this->getLogger()->debug(TextFormat::BOLD.TextFormat::YELLOW.'Notice: Setting pitch of "'.$pitch.'" to -90');
    }
    if($pitch > 90){
      $pitch = 90;
      $this->getLogger()->debug(TextFormat::BOLD.TextFormat::YELLOW.'Notice: Setting pitch of "'.$pitch.'" to 90');
    }
    if($pitch >= -90 AND $pitch <= -60){
      return self::MODE_LEAP;
    }elseif($pitch >= -60 AND $pitch <= 60){
      return self::MODE_CHARGE;
    }elseif($pitch >= 60 AND $pitch <= 90){
      return self::MODE_DASH;
    }
    throw new \UnexpectedValueException("Unexpected pitch '$pitch', found no valid mode!");
  }
}