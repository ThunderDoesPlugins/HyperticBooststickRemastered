<?php
declare(strict_types=1);
/** Created By Thunder33345 **/
namespace Thunder33345\HyperticBoostStick;

require('StringUtils.php');
$text = "Short txt\n".
 "Some text hereish\n".
 "Some longer longer longer longer text\n".
 "Some shorter\n".
 "Long long long long long long long text\n".
 "123";
//$text = "srt\nHello World\n123\n45678";
$out = StringUtils::center($text);
echo $out;